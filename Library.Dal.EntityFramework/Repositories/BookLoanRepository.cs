﻿using Library.Dal.Entities;
using Library.Dal.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Library.Dal.EntityFramework.Repositories
{
    public class BookLoanRepository : IBookLoanRepository
    {
        private readonly LibraryDBContext dBContext;

        public BookLoanRepository(LibraryDBContext dBContext)
        {
            this.dBContext = dBContext;
        }

        public void Add(BookLoan bookLoan)
        {
            dBContext.Add(bookLoan);
        }

        public void Delete(int userId, int bookId)
        {
            var entity = dBContext.BookLoans.FirstOrDefault(bl => bl.BookId == bookId && bl.UserId == userId);
            if (entity != null)
            {
                dBContext.Remove(entity);
            }
        }

        public BookLoan Get(int userId, int bookId)
        {
            var entity = dBContext.BookLoans.AsNoTracking().FirstOrDefault(bl => bl.BookId == bookId && bl.UserId == userId);
            return entity;
        }

        public int GetUserLoanCount(int userId)
        {
            return dBContext.BookLoans.AsNoTracking().Count(bl => bl.UserId == userId);
        }
    }
}
