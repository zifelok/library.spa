﻿using Library.BL.Interfaces.Dto.User;
using Library.BL.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Library.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly Configuration.Jwt jwtConfiguration;
        private readonly IUserService userService;

        public TokenController(Microsoft.Extensions.Configuration.IConfiguration configuration, IUserService userService)
        {
            jwtConfiguration = configuration.GetSection("Jwt").Get<Configuration.Jwt>();
            this.userService = userService;
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult CreateToken(LoginDto loginDto)
        {
            IActionResult response = Unauthorized();
            var user = userService.Login(loginDto);

            if (user != null)
            {
                var tokenString = BuildToken(user);
                response = Ok(new { token = tokenString });
            }

            return response;
        }

        private string BuildToken(LoginResultDto user)
        {
            var claims = new List<Claim> {
                new Claim(JwtRegisteredClaimNames.NameId, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName)
            };

            foreach (var role in user.Roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtConfiguration.Key));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(jwtConfiguration.Issuer,
              jwtConfiguration.Issuer,
              claims,
              expires: DateTime.Now.AddMinutes(30),
              signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        //for testing
        [Authorize]
        [HttpGet("authorized")]
        public object IsAuthorized()
        {
            var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
            return new { userId };
        }

        //for testing
        [Authorize(Roles ="Administrator")]
        [HttpGet("administrator")]
        public object IsAdministrator()
        {
            var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
            return new { userId };
        }
    }
}