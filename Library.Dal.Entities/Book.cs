﻿using System.Collections.Generic;

namespace Library.Dal.Entities
{
    public class Book
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public int Pages { get; set; }
        public int Quantity { get; set; }
        public string Genre { get; set; }
        public virtual ICollection<BookLoan> BookLoans { get; set; }
    }
}
