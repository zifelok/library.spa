﻿using AutoMapper;
using Library.BL.Interfaces.Dto;
using Library.Dal.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.BL
{
    public static class AutoMapperBLConfiguration
    {
        public static void ConfigureBL(this IMapperConfigurationExpression mapperCofiguration)
        {
            ConfigureBookMapping(mapperCofiguration);
        }

        private static void ConfigureBookMapping(IMapperConfigurationExpression mc)
        {
            mc.CreateMap<BookInputDto, Book>();
            mc.CreateMap<Book, BookDto>();
            mc.CreateMap< Library.BL.Interfaces.Dto.BookDto, Library.Dal.Interfaces.Dto.BookDto>().ReverseMap();
        }
    }
}
