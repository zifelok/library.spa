﻿using Library.Dal.Entities;
using System.Collections.Generic;

namespace Library.Dal.Interfaces
{
    public interface IUserRepository
    {
        User Get(int id);
        User Get(string userName);
        IReadOnlyCollection<Role> GetUserRoles(int userId);
    }
}
