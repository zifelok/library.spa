export interface BookEditModel {
  id?: number;
  title?: string;
  author?: string;
  pages?: number;
  quantity?: number;
  genre?: string;
}
