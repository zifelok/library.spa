import {Injectable} from '@angular/core';
import {ApiClientHelper} from './api-client-helper';
import {Book} from '../models/book/Book';
import {BookEditModel} from "../models/book/BookEditModel";

@Injectable({
  providedIn: 'root'
})
export class BookService {
  constructor(private apiClientHelper: ApiClientHelper) {
  }

  public getBooks(orderby: string, asc: boolean): Promise<Book[]> {
    const url = `/books?orderby=${orderby}&asc=${asc}`;
    return this.apiClientHelper.get<Book[]>(url).toPromise();
  }

  public deleteBook(id: number): Promise<{}> {
    const url = `/books/${id}`;
    return this.apiClientHelper.delete<{}>(url).toPromise();
  }

  public loan(id: number): Promise<{}> {
    const url = `/books/${id}/loan`;
    return this.apiClientHelper.post<{}>(url, {}).toPromise();
  }

  public return(id: number): Promise<{}> {
    const url = `/books/${id}/return`;
    return this.apiClientHelper.post<{}>(url, {}).toPromise();
  }

  public edit(book: BookEditModel): Promise<{}> {
    const url = `/books/${book.id}`;
    return this.apiClientHelper.put<{}>(url, book).toPromise();
  }

  public create(book: BookEditModel): Promise<{}> {
    const url = '/books';
    return this.apiClientHelper.post<{}>(url, book).toPromise();
  }

  public get(id: number): Promise<Book> {
    const url = `/books/${id}`;
    return this.apiClientHelper.get<Book>(url).toPromise();
  }
}
