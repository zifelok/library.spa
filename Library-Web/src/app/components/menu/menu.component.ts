import {Component, OnInit, Output} from '@angular/core';
import {UserService} from '../../services/user-service';
import {StorageService} from '../../services/storage-service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.less']
})
export class MenuComponent implements OnInit {
  @Output()
  public userChecked = false;
  @Output()
  public isAuthenticated = false;
  @Output()
  public userName = '';

  constructor(private userService: UserService,
              private storageService: StorageService,
              private router: Router) {
    this.refresh();
  }

  ngOnInit() {

  }

  private refresh() {
    this.userService.getCurrentUser().then(u => {
      this.userChecked = true;
      this.isAuthenticated = this.userService.isAuthenticated();
      if (this.isAuthenticated) {
        this.userName = u.userName;
      }
    });
  }

  public logout() {
    this.storageService.setToken(null);
    this.userService.getCurrentUser(true);
    this.isAuthenticated = false;
    // TODO: implement backend
  }
}
