﻿using Library.Dal.Entities;
using Library.Dal.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Library.Dal.EntityFramework.Repositories
{
    public class UserRepository : IUserRepository
    {
        public LibraryDBContext context;

        public UserRepository(LibraryDBContext context)
        {
            this.context = context;
        }

        public User Get(int id)
        {
            return context.Users.AsNoTracking().FirstOrDefault(u => u.Id == id);
        }

        public User Get(string userName)
        {
            return context.Users.AsNoTracking().FirstOrDefault(u => u.UserName == userName);
        }

        public IReadOnlyCollection<Role> GetUserRoles(int userId)
        {
            return context.UserRoles.AsNoTracking().Where(ur => ur.UserId == userId).Select(ur => ur.Role).ToList();
        }
    }
}
