import {Component, OnDestroy, OnInit, Output, Input} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {BookEditModel} from '../../models/book/BookEditModel';
import {BookService} from '../../services/books-service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.less']
})
export class EditComponent implements OnInit, OnDestroy {
  private sub: any;
  private id: number;
  @Output()
  public title = 'Create';
  @Input()
  public book: BookEditModel = {};

  constructor(private route: ActivatedRoute,
              private bookService: BookService,
              private router: Router) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
      if (this.id) {
        this.title = 'Edit';
        this.loadBook();
      } else {
        this.title = 'Create';
      }
      // In a real app: dispatch action to load the details here.
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  public save() {
    if (this.book.id) {
      this.bookService.edit(this.book).then(this.navigateHome);
    } else {
      this.bookService.create(this.book).then(this.navigateHome);
    }
  }

  private navigateHome = () => this.router.navigate(['/']);
  private loadBook = () => this.bookService.get(this.id).then(b => this.book = b);
}
