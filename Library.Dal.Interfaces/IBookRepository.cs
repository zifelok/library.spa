﻿using Library.Dal.Entities;
using Library.Dal.Interfaces.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Dal.Interfaces
{
    public interface IBookRepository
    {
        IReadOnlyCollection<BookDto> GetBookDtos(string orderby = null, bool asc = true, int? userId = null);

        Book Get(int id);

        Book Add(Book book);

        void Delete(int id);
        void Update(Book entity);
    }
}
