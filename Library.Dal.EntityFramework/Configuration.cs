﻿using System;
using Library.Dal.Entities;
using Microsoft.EntityFrameworkCore;

namespace Library.Dal.EntityFramework
{
    internal static class Configuration
    {
        public static void Configure(ModelBuilder modelBuilder)
        {
            ConfigureUserRoles(modelBuilder);
            ConfigureBook(modelBuilder);
            ConfigureBookLoan(modelBuilder);
        }

        private static void ConfigureBookLoan(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BookLoan>().HasKey(bl => new { bl.BookId, bl.UserId });
            modelBuilder.Entity<BookLoan>().HasOne(bl => bl.User).WithMany(u => u.BookLoans);
            modelBuilder.Entity<BookLoan>().HasOne(bl => bl.Book).WithMany(b => b.BookLoans);
        }

        private static void ConfigureUserRoles(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserRole>().HasKey(ur => new { ur.RoleId, ur.UserId });
            modelBuilder.Entity<UserRole>().HasOne(ur => ur.User).WithMany(u => u.UserRoles);
            modelBuilder.Entity<UserRole>().HasOne(ur => ur.Role).WithMany(r => r.UserRoles);
        }

        private static void ConfigureBook(ModelBuilder modelBuilder)
        {

        }
    }
}
