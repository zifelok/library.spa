﻿using Library.BL.Interfaces.Dto.User;
using Library.BL.Interfaces.Services;
using Library.Dal.Entities;
using Library.Dal.Interfaces;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Library.BL.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository userRepository;

        public UserService(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        public LoginResultDto Login(LoginDto loginDto)
        {
            var user = userRepository.Get(loginDto.UserName);
            if (user != null)
            {
                var loginHash = GenerateHash(loginDto.Password, user.PasswordSalt);
                if (loginHash == user.PasswordHash)
                {
                    var roles = userRepository.GetUserRoles(user.Id);
                    return new LoginResultDto
                    {
                        Id = user.Id,
                        UserName = user.UserName,
                        Roles = roles.Select(r => r.Title).ToList()
                    };
                }
            }
            return null;
        }

        public UserDto GetUser(int id)
        {
            var user = userRepository.Get(id);
            if (user == null)
                return null;
            var roles = userRepository.GetUserRoles(user.Id);
            return new UserDto
            {
                Id = user.Id,
                UserName = user.UserName,
                Roles = roles.Select(r => r.Title).ToList()
            };
        }

        private static string GenerateHash(string password, string salt)
        {
            return GenerateHash(password + salt);
        }

        private static string GeneratetSalt()
        {
            byte[] bytes = new byte[128 / 8];
            using (var keyGenerator = RandomNumberGenerator.Create())
            {
                keyGenerator.GetBytes(bytes);
                return BitConverter.ToString(bytes).Replace("-", "").ToLower();
            }
        }

        private static string GenerateHash(string text)
        {
            // SHA512 is disposable by inheritance.  
            using (var sha256 = SHA256.Create())
            {
                // Send a sample text to hash.  
                var hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(text));
                // Get the hashed string.  
                return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
            }
        }
    }
}
