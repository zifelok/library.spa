export interface Book {
  id: number;
  title: string;
  author: string;
  pages: number;
  quantity: number;
  loanedQuantity: number;
  genre: string;
  owns: boolean;
}
