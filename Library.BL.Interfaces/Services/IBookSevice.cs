﻿using Library.BL.Interfaces.Dto;
using System.Collections.Generic;

namespace Library.BL.Interfaces.Services
{
    public interface IBookSevice
    {
        IReadOnlyCollection<BookDto> Get(string orderby = null, bool asc = true, int? userId = null);
        void Delete(int id);
        BookDto Get(int id);
        void Loan(int id, int userId);
        void Return(int id, int userId);
        BookDto Update(BookInputDto book);
        BookDto Add(BookInputDto book);
    }
}
