import {Component, Input, Output, OnInit} from '@angular/core';
import {BookService} from '../../services/books-service';
import {Book} from '../../models/book/Book';
import {UserService} from "../../services/user-service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {
  @Output()
  public books: Book[];
  @Output()
  public orderby = 'Genre';
  @Output()
  public asc = true;

  constructor(private bookService: BookService,
              private userService: UserService) {
  }

  ngOnInit() {
    this.loadBooks();
  }

  private loadBooks = () => {
    this.bookService.getBooks(this.orderby, this.asc).then(d => this.books = d);
  }

  public order(orderby: string) {
    if (this.orderby === orderby) {
      this.asc = !this.asc;
    } else {
      this.asc = true;
      this.orderby = orderby;
    }
    this.loadBooks();
  }

  public isAdministrator = () => this.userService.isAdministrator();
  public isUser = () => this.userService.isUser();

  public delete(id: number) {
    this.bookService.deleteBook(id).then(this.loadBooks);
  }

  public loanOrReturn(id: number) {
    const owns = this.books.filter(b => b.id === id)[0].owns;
    if (owns) {
      this.bookService.return(id).then(this.loadBooks);
    } else {
      this.bookService.loan(id).then(this.loadBooks);
    }
  }

  public bookAvailableToLoan(book: Book): boolean {
    return book.owns || (book.loanedQuantity < book.quantity && this.books.filter(b => b.owns).length < 3);
  }
}
