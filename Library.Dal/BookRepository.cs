﻿using Library.Dal.Entities;
using Library.Dal.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Dal
{
    public class BookRepository : IBookRepository
    {
        private static List<Book> storage = new List<Book>();
        private static int id = 0;

        public IReadOnlyCollection<Book> Get(string author, string title)
        {
            IEnumerable<Book> result = storage;
            if (!string.IsNullOrWhiteSpace(author))
            {
                author = author.ToLowerInvariant();
                result = result.Where(b => b.Author.ToLowerInvariant().Contains(author));
            }
            if (!string.IsNullOrWhiteSpace(title))
            {
                title = title.ToLowerInvariant();
                result = result.Where(b => b.Title.ToLowerInvariant().Contains(title));
            }
            return result.ToList();
        }

        public Book Get(int id)
        {
            return storage.FirstOrDefault(b => b.Id == id);
        }

        public Book Add(Book book)
        {
            book.Id = ++id;
            storage.Add(book);
            return book;
        }

        public void Delete(int id)
        {
            var book = Get(id);
            if (book != null)
                storage.Remove(book);
        }
    }
}
