﻿using AutoMapper;
using Library.Dal.Entities;
using Library.Dal.Interfaces.Dto;

namespace Library.BL
{
    public static class AutoMapperDalConfiguration
    {
        public static void ConfigureDal(this IMapperConfigurationExpression mapperCofiguration)
        {
            ConfigureBookMapping(mapperCofiguration);
        }

        private static void ConfigureBookMapping(IMapperConfigurationExpression mc)
        {
            mc.CreateMap<Book, BookDto>();
        }
    }
}
