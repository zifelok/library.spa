﻿using System;
using Library.Dal.Entities;
using Microsoft.EntityFrameworkCore;

namespace Library.Dal.EntityFramework
{
    public class LibraryDBContext : DbContext
    {
        public LibraryDBContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Book> Books { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<BookLoan> BookLoans { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            Configuration.Configure(modelBuilder);
            Seed.SeedData(modelBuilder);
        }
    }
}
